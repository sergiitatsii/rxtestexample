## RxTestExample

RxTestExample is a handy app to give you the red, green, and blue values and color name (if available) for the hex color code you enter. This app is organized using the MVVM design pattern.

### Compiling

You need to install dependencies from CocoaPods:

```bash
pod install
```

Open RxTestExample.xcworkspace, and compile.
